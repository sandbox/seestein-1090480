// $Id$
if (Drupal.jsEnabled) {
  /*
   * 
   * 
   */
  //alert($().jquery);
  if (typeof $(document).live == 'function') {
  $(document).ready(function() {
	var content = new Array();
	$("div.edit-custom-view").bind({
	  click: function() {
		if (!$(this).hasClass("edit-now")) {
			var id = $(this).attr("id");
			content[id] = $(this).html();
			var nids = id.split("-");
			var nid = nids[nids.length-1];
			$(this).addClass("edit-now");
			var width = $(this).width();
			var height = $(this).height();
			var type = $(this).attr("class");
			type = type.replace(/edit-custom-view/g,"");
			type = type.replace(/edit-now/g,"");
			type = type.replace(/edit-table/g,"");
			$.ajax({
		      type: 'POST',
		      url: '/content_edit/edit',
		      beforeSend: function(){
		    	  $("#"+id).prepend("<span class='content-edit-save'></span>");
			  },
		      success: function(data) {
		    	if (data!=0) {
		    	  $("#"+id).html(data);
		    	} else {
			      $("#"+id+" .content-edit-save").remove();
			      $("#"+id).removeClass("edit-now");
		    	}
		      },
		      dataType: 'html',
		      data: [{name:"text",value:$(this).html()}, {name:"type",value:type}, {name:"nid",value:nid}, {name:"width",value:width}, {name:"height",value:height}, {name:"id",value:$(this).attr("id")}]
		    });
		}
	  },
	  mouseover: function() {
		if (!$(this).hasClass("edit-now"))
			$(this).addClass("edit-table");
	  },
	  mouseout: function() {
		if (!$(this).hasClass("edit-now"))
			$(this).removeClass("edit-table");
	  }
	});

	$("input.save-button").live("click", function(){
		var id = $(this).attr("id").replace("-save","");
		var nids = id.split("-");
		var nid = nids[nids.length-1];
		$.ajax({
	      type: 'POST',
	      url: '/content_edit/save',
	      beforeSend: function(){
			$("#"+id+"-save").after("<span class='content-edit-save'></span>");
		  },
	      success: function(data) {
	    	$("#"+id).html(data);
	    	$("#"+id).removeClass("edit-table");
	  		$("#"+id).removeClass("edit-now");
	      },
	      dataType: 'html',
	      data: [{name:"text",value:$("#"+id+'-textarea').val()}, {name:"nid",value:nid}, {name:"id",value:id}]
	    });
	});
	
	$("input.cancel-button").live("click", function(){
		var id = $(this).attr("id").replace("-cancel","");
		var text = content[id];
		delete content[id];
		text = text.replace(/&lt;/g,"<");
		text = text.replace(/&gt;/g,">");
		$("#"+id).html(text);
		$("#"+id).removeClass("edit-table");
		$("#"+id).removeClass("edit-now");
	});
  });
  }
}