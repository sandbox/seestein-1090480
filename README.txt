// $Id$
Russian

Модуль Content Edit позволяет редактировать материалы, выводимые с помощью view типа 'Node',
используя представления page, block.
Можно редактировать все пользовательские поля следующих типов:
1) текстовая область;
2) строка;
3) число (Intenger,Float etc.);
4) дата.
Так же можно редактировать системные поля:
1) заголовок (Title);
2) содержимое (body);
3) дата создания, дата обновления (created, changed).
Чтобы начать редактировать поле, если оно доступно для редактирования, нужно по нему кликнуть мышью, в результате
чего появится форма для редактирования.
Чтобы пользователь смог использовать этот модуль, необходимо его роли дать право на использование этого модуля,
дать право на редактирование нужного типа материала.
Работает на Ajax. Требуется JQuery 1.4.3 или выше. После установки модуля сбросте кеш. Не работает для мультиполей.

English

Module Content Edit lets you edit the materials displayed by using the view type 'Node',
using the display page, block.
You can edit all the custom fields of the following types:
1) text field;
2) string;
3) number (Intenger, Float etc.);
4) date.
You can also edit the system fields:
1) title;
2) body;
3) created, changed.
To begin the edit field if it is available for editing, you need to click on it, resulting in a form to edit content.
That the user can use this module, give his role the access for using this module and editing the desired type of material.
Powered by Ajax. Required JQuery 1.4.3 or higher. After installing the module refresh cache. Does not work for the multifields.

TODO:

1) add the ability to edit ImageField;
2) add resize textarea;
3) maybe support a visual HTML editor.